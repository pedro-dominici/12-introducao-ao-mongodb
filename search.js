

// node serach.js "Nome do aluno" "Curso" "Telefone"; Pode ser pesquisado somente por nome 
// se quiser um refinamento pode se colocar Curso e Telefone


const mongo = require('mongodb')
const {MongoClient} = mongo;

MongoClient.connect("mongodb://localhost:27017" ,(err,client) =>{
    if (err) throw err;
    const db = client.db("pedro")

    db.collection("alunos").findOne({
        nome: {$regex: new RegExp(process.argv[2])}, 
        curso: {$regex: new RegExp(process.argv[3])},
        telefone: {$regex: new RegExp(process.argv[4])}}, 
        (err,res) =>{

        console.log(res)
        client.close()
    })
})
