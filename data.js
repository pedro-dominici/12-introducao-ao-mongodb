const data =[
    {
        nome: "Pedro Henrique",
        idade: 18,
        curso: "ADS",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Maria Julia",
        idade: 20,
        curso: "ADS",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Gustavo",
        idade: 23,
        curso: "GTI",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Fernando",
        idade: 25,
        curso: "GTI",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Claudio",
        idade: 18,
        curso: "GTI",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Julia Almeida",
        idade: 23,
        curso: "ADS",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Fernanda",
        idade: 19,
        curso: "ADS",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Felipe",
        idade: 30,
        curso: "ADS",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Anderson",
        idade: 25,
        curso: "GTI",
        semestre: "1 Semestre",
        telefone: "912341234",
    },
    {
        nome: "Julio",
        idade: 19,
        curso: "ADS",
        semestre: "1 Semestre",
        telefone: "912341234",
    },]
module.exports = data