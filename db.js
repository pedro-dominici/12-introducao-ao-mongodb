const mongo = require('mongodb');
const { MongoClient } = mongo;
const url = "mongodb://localhost:27017/";

//criar bd

MongoClient.connect("mongodb://localhost:27017/pedro", (err, client) => {
    if (err) throw err;
    console.log("Database created!");
    client.close();
});

//Criar coleção com nome users

MongoClient.connect(url, (err, client) => {
    if (err) throw err;
    const dbo = client.db("pedro");
    dbo.createCollection("alunos", (err, res) => {
        if (err) throw err;
        console.log("✅ Collection created!");
        client.close();
    });
});
