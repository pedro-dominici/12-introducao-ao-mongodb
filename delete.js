
//node delete.js "Nome do aluno a ser deletado"

const mongo = require('mongodb')
const {MongoClient} = mongo;

MongoClient.connect("mongodb://localhost:27017",(err,client) =>{
    if (err) throw err;
    const db = client.db("pedro")

    db.collection("alunos").deleteOne({nome: process.argv[2]}, (err, res) =>{
        console.log(res)
        client.close()
    })
})