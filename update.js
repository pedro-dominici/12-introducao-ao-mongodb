
//node update.js "Nome completo a ser alterado" "Novo nome"

const mongo = require('mongodb')
const {MongoClient} = mongo;

MongoClient.connect("mongodb://localhost:27017",(err,client) =>{
    if (err) throw err;
    const db = client.db("pedro")

    db.collection("alunos").updateOne({nome: process.argv[2]}, {'$set': {'nome':process.argv[3]}}, (err, res) =>{
        console.log(res)
        client.close()
    })
})