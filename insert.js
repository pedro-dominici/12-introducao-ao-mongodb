const mongo = require('mongodb')
const {MongoClient} = mongo;
const data = require('./data')

MongoClient.connect("mongodb://localhost:27017", (err,client) =>{

    if(err) throw err;
    const db = client.db('pedro')
    db.collection("alunos").insertMany(data, (err,res)=>{
        if (err) throw err;
        console.log("✅ Usuários cadastrados!")
        client.close();
    })

})