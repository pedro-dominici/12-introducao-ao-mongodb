### MongoDB
#### Segue as instruções

**Criar BD** `node db.js` esse comando ira criar um banco de dados Pedro com uma Collection Alunos
**Inserir dados** `node insert.js` esse comando ira inserir 10 alunos na collection alunos, esses 10 alunos serão puxados do `data.js`
**Procurar** `node search.js "1" "2" "3"` Esse comando ira pesquisar os alunos, onde 1 seria o nome, 2 o curso e 3 o telefone, 2 e 3 são para refinar a pesquisa
**Atualizar** `node update.js "1" "2"` Esse comando ira atualizar o dado, onde 1 é o nome a ser atualizado e 2 sera o novo nome
**Deletar** `node delete.js "1"` Esse comando ira excluir o dado selecionado, onde 1 é o nome do aluno a ser excluido